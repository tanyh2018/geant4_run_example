//
/// \file SiCEDPrimaryGeneratorAction.cc//源文件
//

#include "SiCEDPrimaryGeneratorAction.hh"//头文件

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "math.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiCEDPrimaryGeneratorAction::SiCEDPrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0)
{


  // default particle kinematic   find electron
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();//创建粒子数据库G4particleTable以从中查找对应粒子
  G4String particleName;//定义字符串particlename
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="proton");//从创建的G4particleTable中查找名为"e-"的粒子

  // define beam/particle as one electron with energy position and direction
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);//这两行一起创建粒子
  fParticleGun->SetParticleDefinition(particle);//指定G4particleGun将要产生的粒子为刚刚找到的e-粒子
  fParticleGun->SetParticleEnergy(80*MeV);
  fParticleGun->SetParticlePosition(G4ThreeVector(0*cm,0*cm,0*cm));//发射位置
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0*cm,0.*cm,1*cm));//发射方向

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiCEDPrimaryGeneratorAction::~SiCEDPrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SiCEDPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  // G4double direction_theta = acos(G4UniformRand());                       
  // if( G4UniformRand() < 0.5){ direction_theta = M_PI-direction_theta; };
  // G4double direction_phi = G4UniformRand()*M_PI*2;

  // G4double direction_x = sin(direction_theta)*cos(direction_phi)*cm;
  // G4double direction_y = sin(direction_theta)*sin(direction_phi)*cm;
  // G4double direction_z = cos(direction_theta)*cm;
  fParticleGun->GeneratePrimaryVertex(anEvent);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


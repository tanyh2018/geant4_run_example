//
/// \file SiCEDActionInitialization.cc
/// \brief Implementation of the SiCEDActionInitialization class
//由于使用多线程，必须引入
#include "SiCEDActionInitialization.hh"
#include "SiCEDPrimaryGeneratorAction.hh"//指定，第一个步骤 ：头文件（多线程）
#include "SiCEDRunAction.hh"
#include "SiCEDEventAction.hh"//在这里指定event需要两步：1.头文件2.在成员函数中连接
#include "SiCEDSteppingAction.hh"
#include "SiCEDDetectorConstruction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiCEDActionInitialization::SiCEDActionInitialization
                          (SiCEDDetectorConstruction* detConstruction)
 : G4VUserActionInitialization(),
   fDetConstruction(detConstruction)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiCEDActionInitialization::~SiCEDActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SiCEDActionInitialization::BuildForMaster() const
{
  SiCEDRunAction* runAction = new SiCEDRunAction;
  SetUserAction(runAction);        
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SiCEDActionInitialization::Build() const
{
  //define a beam particle
  SetUserAction(new SiCEDPrimaryGeneratorAction);   //将PrimaryGenerator在这里指定，需要的第二个步骤：通过new动态创建PrimaryGenerator
                                                    // 类，并通过SetUserAction方法指定其为Gent4程序中的粒子源

  SiCEDRunAction* runAction = new SiCEDRunAction;
  SetUserAction(runAction);
  
  SiCEDEventAction* eventAction = new SiCEDEventAction(runAction);//连接event的第二个步骤（创建类)
  SetUserAction(eventAction);                                     //连接event的第二个步骤（指定其为Gent4程序的EventAction
  
  SetUserAction(new SiCEDSteppingAction(fDetConstruction,eventAction));
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

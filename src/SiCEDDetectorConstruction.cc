//
/// \file SiCEDDetectorConstruction.cc
//

#include "SiCEDDetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4RotationMatrix.hh"
#include "G4Element.hh"
#include "G4SubtractionSolid.hh"
#include "G4UserLimits.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiCEDDetectorConstruction::SiCEDDetectorConstruction()
: G4VUserDetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiCEDDetectorConstruction::~SiCEDDetectorConstruction()
{
 }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* SiCEDDetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
  
  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;
  //G4Material* device_mat = nist->FindOrBuildMaterial("G4_Si");

  //
  //  material define
  //
  G4Element* silicon_element = nist->FindOrBuildElement("Si");
  G4Element* oxygen_element = nist->FindOrBuildElement("O");
  G4Element* carbide_element = nist->FindOrBuildElement("C");
  G4Element* hydrogenium_element = nist->FindOrBuildElement("H");

  G4Material* Cu_mat = nist->FindOrBuildMaterial("G4_Cu");

  //pcb
  G4double pcb_density = 1.9*g/cm3;
  G4Material* pcb_mat =  new G4Material("PCB",pcb_density,2);
  pcb_mat->AddElement(silicon_element,1);
  pcb_mat->AddElement(oxygen_element,2); 
  
  // ep
  G4double ep_density = 1.2*g/cm3;
  G4Material* ep_mat =  new G4Material("ep",ep_density,3);
  ep_mat->AddElement(carbide_element,11); 
  ep_mat->AddElement(hydrogenium_element,12);
  ep_mat->AddElement(oxygen_element,3); 

  // The size of Cu and pcb
  G4double cu_sizeX = 2*cm, cu_sizeY = 2*cm, cu_sizeZ = 35.*um;
  G4double pcb_sizeX = 2*cm, pcb_sizeY = 2*cm, pcb_sizeZ = 1.53*mm;

  // World
  G4double world_sizeXY = 4*cm , world_sizeZ = 4.0*cm + pcb_sizeZ + cu_sizeZ*2.0;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
 
  G4Box* world_box = new G4Box("World", 
                                0.5*world_sizeXY, 
                                0.5*world_sizeXY, 
                                0.5*world_sizeZ);

  G4LogicalVolume* world_logic = new G4LogicalVolume(world_box,            
                                                    world_mat,            
                                                    "World");   

  G4VPhysicalVolume* world_phy = new G4PVPlacement(0,              
                                                  G4ThreeVector(),
                                                  world_logic,     
                                                  "World",        
                                                  0,              
                                                  false,          
                                                  0,              
                                                  checkOverlaps);

    //pcb1
	G4double pcb1_z_position = 2*cm;	
  	G4ThreeVector pcb1_pos = G4ThreeVector(0, 0, pcb1_z_position);

  	G4Box* pcb1_box = new G4Box("PCB1",
                                0.5*pcb_sizeX,
                                0.5*pcb_sizeY,
                                0.5*pcb_sizeZ); 

  	G4LogicalVolume* pcb1_logic = new G4LogicalVolume(pcb1_box,         
                                                  ep_mat,          
                                                  "PCB1_Board");

  	G4PVPlacement* pcb1_pv = new G4PVPlacement(0,                 
                                            pcb1_pos,              
                                            pcb1_logic,             
                                            "PCB1_Board",                
                                            world_logic,             
                                            false,                  
                                            0,                      
                                            checkOverlaps); 

	// Cu1 top
	G4double cu1_top_z_position = pcb1_z_position - pcb_sizeZ*0.5 - cu_sizeZ*0.5;

	G4ThreeVector cu1_top_pos = G4ThreeVector(0, 0, cu1_top_z_position);

	G4Box* cu1_top_box = new G4Box("cu1_top",
									0.5*cu_sizeX,
									0.5*cu_sizeY,
									0.5*cu_sizeZ); 

	G4LogicalVolume* cu1_top_logic = new G4LogicalVolume(cu1_top_box,         
													Cu_mat,          
													"cu1_top_Board");

	G4PVPlacement* cu1_top_pv = new G4PVPlacement(0,                 
												cu1_top_pos,              
												cu1_top_logic,             
												"cu1_top_Board",                
												world_logic,             
												false,                  
												0,                      
												checkOverlaps);  
   // Cu1 bottom
	G4double cu1_bottom_z_position = pcb1_z_position + pcb_sizeZ*0.5 + cu_sizeZ*0.5;
	std::cout<<"cu1_bottom_z_position:"<<cu1_bottom_z_position<<std::endl;

	G4ThreeVector cu1_bottom_pos = G4ThreeVector(0, 0, cu1_bottom_z_position);

	G4Box* cu1_bottom_box = new G4Box("cu1_bottom",
									0.5*cu_sizeX,
									0.5*cu_sizeY,
									0.5*cu_sizeZ); 

	G4LogicalVolume* cu1_bottom_logic = new G4LogicalVolume(cu1_bottom_box,         
													Cu_mat,          
													"cu1_bottom_Board");

	G4PVPlacement* cu1_bottom_pv = new G4PVPlacement(0,                 
												cu1_bottom_pos,              
												cu1_bottom_logic,             
												"cu1_bottom_Board",                
												world_logic,             
												false,                  
												0,                      
												checkOverlaps); 

	//pcb2
	G4double pcb2_z_position = 1*cm;	
	G4ThreeVector pcb2_pos = G4ThreeVector(0, 0, pcb2_z_position);

	G4Box* pcb2_box = new G4Box("PCB2",
									0.5*pcb_sizeX,
									0.5*pcb_sizeY,
									0.5*pcb_sizeZ); 

	G4LogicalVolume* pcb2_logic = new G4LogicalVolume(pcb2_box,         
													ep_mat,          
													"PCB2_Board");

	G4PVPlacement* pcb2_pv = new G4PVPlacement(0,                 
												pcb2_pos,              
												pcb2_logic,             
												"PCB2_Board",                
												world_logic,             
												false,                  
												0,                      
                                            checkOverlaps); 
	// Cu2 top
	G4double cu2_top_z_position = pcb2_z_position - pcb_sizeZ*0.5-cu_sizeZ*0.5;

	G4ThreeVector cu2_top_pos = G4ThreeVector(0, 0, cu2_top_z_position);

	G4Box* cu2_top_box = new G4Box("cu2_top",
									0.5*cu_sizeX,
									0.5*cu_sizeY,
									0.5*cu_sizeZ); 

	G4LogicalVolume* cu2_top_logic = new G4LogicalVolume(cu2_top_box,         
													Cu_mat,          
													"cu2_top_Board");

	G4PVPlacement* cu2_top_pv = new G4PVPlacement(0,                 
												cu2_top_pos,              
												cu2_top_logic,             
												"cu2_top_Board",                
												world_logic,             
												false,                  
												0,                      
												checkOverlaps);  

	// Cu2 bottom
	G4double cu2_bottom_z_position = pcb2_z_position + pcb_sizeZ*0.5 + cu_sizeZ*0.5;

	G4ThreeVector cu2_bottom_pos = G4ThreeVector(0, 0, cu2_bottom_z_position);

	G4Box* cu2_bottom_box = new G4Box("cu2_bottom",
									0.5*cu_sizeX,
									0.5*cu_sizeY,
									0.5*cu_sizeZ); 

	G4LogicalVolume* cu2_bottom_logic = new G4LogicalVolume(cu2_bottom_box,         
													Cu_mat,          
													"cu2_bottom_Board");

	G4PVPlacement* cu2_bottom_pv = new G4PVPlacement(0,                 
												cu2_bottom_pos,              
												cu2_bottom_logic,             
												"cu2_bottom_Board",                
												world_logic,             
												false,                  
												0,                      
												checkOverlaps); 


  G4double maxStep = 10.0*um;
  fStepLimit = new G4UserLimits(maxStep);
  world_logic->SetUserLimits(fStepLimit);


  return world_phy;
}

void SiCEDDetectorConstruction::SetMaxStep(G4double maxStep)
{
  if ((fStepLimit)&&(maxStep>0.)) fStepLimit->SetMaxAllowedStep(maxStep);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
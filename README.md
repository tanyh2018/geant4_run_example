# SiCED
Gent4 simulation project: 80MeV proton beam rest energy after two PCB board 

## Setup

ROOT verion 6, Geant4 version 4.10.04

## Build

cmake .

make

## Run

./SiCED run.mac

## Analysis
 
Edep_device branch in SiCED_5um.root is the result
